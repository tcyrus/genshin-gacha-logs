import json

from mitmproxy import command, ctx, http

GENSHIN_WEBSTATIC_HOST = "webstatic-sea.mihoyo.com"
GENSHIN_HK4E_API_HOST = "hk4e-api-os.mihoyo.com"

GENSHIN_WEBSTATIC_GACHA_INDEX_PATH = "/hk4e/event/e20190909gacha/"
GENSHIN_WEBSTATIC_GACHA_INFO_PATH = "/hk4e/gacha_info/os_usa/items/en-us.json"

GENSHIN_HK4E_API_CONFIG_LIST_PATH = "/event/gacha_info/api/getConfigList"
GENSHIN_HK4E_API_GACHA_LOG_PATH = "/event/gacha_info/api/getGachaLog"

class GenshinImpact:
    def __init__(self) -> None:
        self.auth_params = {}
        self.auth_cookies = {}
        self.gacha_info = []
        self.gacha_conf = []
        self.gacha_logs = {}

    def request(self, flow: http.HTTPFlow):
        if flow.request.pretty_host == GENSHIN_WEBSTATIC_HOST:
            if flow.request.path.startswith(GENSHIN_WEBSTATIC_GACHA_INDEX_PATH):
                self.auth_params = flow.request.query
                # self.auth_params = flow.request._get_query()
                self.auth_cookies = flow.request.cookies
                # self.auth_cookies = flow.request._get_cookies()
                ctx.log.info("Auth Credentials Retrieved")

    @command.command("genshin.load")
    def save_output_data(self) -> None:
        with open("data/auth_params.json", "r") as fp:
            self.auth_params = json.load(fp)
        with open("data/auth_cookies.json", "r") as fp:
            self.auth_cookies = json.load(fp)
        with open("data/gacha_info.json", "r") as fp:
            self.gacha_info = json.load(fp)
        with open("data/gacha_conf.json", "r") as fp:
            self.gacha_conf = json.load(fp)
        with open("data/gacha_logs.json", "r") as fp:
            self.gacha_logs = json.load(fp)

    @command.command("genshin.save")
    def save_output_data(self) -> None:
        with open("data/auth_params.json", "w") as fp:
            json.dump(self.auth_params, fp)
        with open("data/auth_cookies.json", "w") as fp:
            json.dump(self.auth_cookies, fp)
        with open("data/gacha_info.json", "w") as fp:
            json.dump(self.gacha_info, fp)
        with open("data/gacha_conf.json", "w") as fp:
            json.dump(self.gacha_conf, fp)
        with open("data/gacha_logs.json", "w") as fp:
            json.dump(self.gacha_logs, fp)

    @command.command("genshin.gatcha_info")
    def spoof_gatcha_info_req(self):
        # TODO: Make Spoof Req
        # ctx.master.commands.call("replay.client", [flow])
        return

    @command.command("genshin.gatcha_conf")
    def spoof_gatcha_conf_req(self):
        # TODO: Make Spoof Req
        # ctx.master.commands.call("replay.client", [flow])
        return

    @command.command("genshin.gatcha_log")
    def spoof_gatcha_log_req(self, gacha_type: int, page: int, size: int):
        # TODO: Make Spoof Req
        # ctx.master.commands.call("replay.client", [flow])
        return

    def response(self, flow: http.HTTPFlow):
        if flow.request.pretty_host == GENSHIN_WEBSTATIC_HOST:
            if flow.request.path.startswith(GENSHIN_WEBSTATIC_GACHA_INFO_PATH):
                self.gacha_info = json.loads(flow.response.text)
                ctx.log.info("Gacha Info Recieved")
        elif flow.request.pretty_host == GENSHIN_HK4E_API_HOST:
            if flow.request.path.startswith(GENSHIN_HK4E_API_CONFIG_LIST_PATH):
                tmp_data = json.loads(flow.response.text)['data']
                self.gacha_conf = tmp_data['gacha_type_list']
                ctx.log.info("Gacha List Recieved")
            elif flow.request.path.startswith(GENSHIN_HK4E_API_GACHA_LOG_PATH):
                tmp_data = json.loads(flow.response.text)['data']
                gacha_type = flow.request.query['gacha_type']
                if gacha_type not in self.gacha_logs:
                    self.gacha_logs[gacha_type] = {}
                start_idx = (int(tmp_data['page'])-1) * int(tmp_data['size'])
                for idx, roll in enumerate(tmp_data['list'], start_idx):
                    self.gacha_logs[gacha_type][str(idx)] = roll
                ctx.log.info("Gacha Log Recieved")

addons = [
    GenshinImpact()
]
