import sqlite3
import json

conn = sqlite3.connect('data/genshin.db')

c = conn.cursor()

c.execute("""
CREATE TABLE IF NOT EXISTS gacha_conf(
    id integer PRIMARY KEY,
    key integer NOT NULL UNIQUE,
    name text NOT NULL
)
""")

c.execute("""
CREATE TABLE IF NOT EXISTS gacha_info(
    item_id integer PRIMARY KEY,
    name text NOT NULL,
    item_type text NOT NULL,
    rank_type text NOT NULL
)
""")

c.execute("""
CREATE TABLE IF NOT EXISTS gacha_logs(
    uid integer NOT NULL,
    gacha_type integer NOT NULL,
    idx integer NOT NULL,
    item_id integer NOT NULL,
    count integer NOT NULL,
    time text NOT NULL,
    PRIMARY KEY(uid, gacha_type, idx),
    FOREIGN KEY(gacha_type) REFERENCES gacha_conf(key),
    FOREIGN KEY(item_id) REFERENCES gacha_info(item_id)
)
""")

conn.commit()

with open("data/gacha_info.json", "r") as fp:
    data = json.load(fp)
    data_ = map(lambda e: (e['item_id'], e['name'], e['item_type'], e['rank_type'],), data)
    c.executemany('INSERT INTO gacha_info VALUES (?,?,?,?)', data_)
    conn.commit()

with open("data/gacha_conf.json", "r") as fp:
    data = json.load(fp)
    data_ = map(lambda e: (e['id'], e['key'], e['name'],), data)
    c.executemany('INSERT INTO gacha_conf VALUES (?,?,?)', data_)
    conn.commit()

with open("data/gacha_logs.json", "r") as fp:
    data = json.load(fp)
    data_ = []
    for _k, val in data.items():
        num_rolls = len(val) - 1
        for idx, roll in val.items():
            idx_ = num_rolls - int(idx)
            data_.append((roll['uid'], roll['gacha_type'], idx, roll['item_id'], roll['count'], roll['time'],))
    c.executemany('INSERT INTO gacha_logs VALUES (?,?,?,?,?,?)', data_)
    conn.commit()
