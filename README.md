# Genshin Gacha Logs

This tool is dedicated to extracting the Gacha Logs from Genshin Impact.

The reason that a tool like this is necessary is because Genshin Impact refuses to open web pages in the default web browser (apart from the Feedback form).

This tool is designed to only deal with the necessary hosts and endpoints. It is not designed to modify requests or any sort of cheating.

```
mitmdump -s ./genshin-mitmproxy.py
python3 genshin-sqlite.py
```
